/**
 * 
 */
package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

// TODO: Auto-generated Javadoc
/**
 * The Class VendingMachineTest.
 *
 * @author hannachang
 */
public class VendingMachineTest {
	
	/**  item  */
	VendingMachineItem item, item1, item2, item3, item4;
	
	/**  vending machine. */
	VendingMachine vendingMachine;

	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		item = new VendingMachineItem("Hershey Bar", 2.00);
		item1 = new VendingMachineItem("Snickers", 2.00);
		item2 = new VendingMachineItem("Potato Chips", 2.00);
		item3 = new VendingMachineItem("Skittles", 2.00);
		item4 = new VendingMachineItem("Pretzels", 2.00);
		vendingMachine = new VendingMachine();
		vendingMachine.addItem(item, "A");
	}
	
	
	@Test
	public void addItemTest()
	{
		/** add item to invalid test */
		
		assertEquals(item, vendingMachine.getItem("A"));
	}
	
	@Test(expected = VendingMachineException.class)
	public void addItemInvTest() {
		
		/** add item to invalid test */
		
		assertEquals(item, vendingMachine.getItem("E"));
	}
	
	@Test
	public void removeItemTest()
	{
		/** remove item test*/
		
		assertEquals(item, vendingMachine.removeItem("A"));
	}
	
	@Test (expected = VendingMachineException.class)
	public void removeItemInvTest()
	{
		
	/**remove item invalid test*/
		
		assertEquals(item, vendingMachine.removeItem("E"));
		assertEquals(item, vendingMachine.removeItem(" "));
	}
	
	@Test
	public void insertMoneyTest() {
		/** insert money test*/
		vendingMachine.insertMoney(2.00);
		assertEquals(2.00, vendingMachine.getBalance(), .01);
	}
	
	@Test (expected = VendingMachineException.class)
	public void insertMoneyInvTest() {
		
		/** insert money invalid test*/
		
		vendingMachine.insertMoney(-1);
		assertEquals(-1, vendingMachine.getBalance(), .01);
	}
	
	@Test
	public void getBalanceTest() {
		/** gets the balance test
		 * @returns balance test
		 */
		vendingMachine.insertMoney(2.00);
		assertTrue(vendingMachine.getBalance()==2.00);
	}
	
	@Test
	public void getBalanceFTest() {
		/** gets balance false test
		 * @returns balance false test
		 */
		vendingMachine.insertMoney(3.00);
		assertFalse(vendingMachine.getBalance()==2.00);
	}
	
	@Test
	public void makePurchaseTest() {
		/** make purchase test */
		vendingMachine.insertMoney(2.00);
		assertTrue(vendingMachine.makePurchase("A"));
	}
	
	@Test
	public void makePurchaseFTest() {
		/** make purchase false test*/
		vendingMachine.insertMoney(1.00);
		assertFalse(vendingMachine.makePurchase("B"));
	}
	
	@Test
	public void returnChangeTest() {
		/** return change test */
		vendingMachine.insertMoney(2.00);
		vendingMachine.getBalance();
		assertTrue(vendingMachine.returnChange() == 2.00);
	}

	@Test
	public void returnChangeFTest() {
		/** return change false test */
		vendingMachine.insertMoney(3.00);
		vendingMachine.getBalance();
		assertFalse(vendingMachine.returnChange() == 5.00);
	}


	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		item = null;
		item1 = null;
		item2 = null;
		item3 = null;
		item4 = null;
		vendingMachine = null;
	}
}
