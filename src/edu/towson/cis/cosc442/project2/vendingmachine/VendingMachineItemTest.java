/**
 * 
 */
package edu.towson.cis.cosc442.project2.vendingmachine;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

// TODO: Auto-generated Javadoc
/**
 * The Class VendingMachineItemTest.
 *
 * @author hannachang
 */
public class VendingMachineItemTest {

	/** The item 4. */
	VendingMachineItem item, item1, item2, item3, item4;
	/**
	 * Sets the up.
	 *
	 * @throws Exception the exception
	 */
	@Before
	public void setUp() throws Exception {
		item = new VendingMachineItem("Hershey Bar", 2.00);
		item1 = new VendingMachineItem("Snickers", 2.00);
		item2 = new VendingMachineItem("Potato Chips", 2.00);
		item3 = new VendingMachineItem("Skittles", 2.00);
		item4 = new VendingMachineItem("Pretzels", 2.00);
	}

	@Test
	public void constructorTest() {
		/**
		 * test constructor
		 */
		VendingMachineItem itemToTest = new VendingMachineItem("Hershey Bar", 2.00);
	}
	
	
	@Test
	public void getNameTest() {
		/** 
		 * tests get name
		 */
		assertEquals("Snickers", item1.getName());
	}
	
	@Test
	public void getNameInvTest() {
		/**
		 * invalid get name test
		 */
		assertFalse(item.getName().equals("Hershey Kiss"));
	}
	
	@Test
	public void getPricetest() {
		/**
		 * tests get price
		 */
		assertEquals(2.00, item.getPrice(), .01);
	}
	
	@Test
	public void getPriceInvTest() {
		/**
		 * invalid get price test
		 */
		assertFalse(item1.getPrice() == 3.00);
	}
	
	/**
	 * Tear down.
	 *
	 * @throws Exception the exception
	 */
	@After
	public void tearDown() throws Exception {
		item = null;
	}
}
